package start;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import logic.ConfigFile;
import logic.Excel;
import logic.GUI_ProgressBar;
import logic.P4parse;
import logic.Parser;
import logic.Utile;

public class Reverse {

	public static void main(String argv[]) throws Exception {
		System.out.println(">> VES Reverse SCWS v0.7 by M-M");
			  
		/**
		 * ===========================================================================
		 *  ------------------------- initialization -------------------------------- 
		 * ===========================================================================
		 **/
		
		System.out.println(" -- initialization -- ");
		ConfigFile conf = new ConfigFile();
		Integer firstRow = conf.getFirstRow();
		Integer firstCell = conf.getFirstCell();
		String unzipPath = conf.getUnzipPath();
		System.out.println("cell : "+firstCell+" row : "+firstRow);
		if (Utile.deleteDirectory(unzipPath+"\\images"))
			System.out.println("----------------del-images----------------");
		if (Utile.deleteDirectory(unzipPath+"\\annotationmetadata"))
			System.out.println("----------------del-annotationmetadata----------------");
		if (Utile.deleteDirectory(unzipPath))
			System.out.println("----------------del-----------------");
				  
		// choose folder
		File choosedFile = null;
		// JFileChooser dialog box
		JFileChooser fc = new JFileChooser(conf.getChoosePath());
		FileNameExtensionFilter filter = new FileNameExtensionFilter("FCW files", "fcw"); // show only fcw files to choose
		fc.setFileFilter(filter);
		int rueckgabeWert = fc.showOpenDialog(null); // open dialog box and look for return statement
			  
		// if user choosed a valid file? 
		if(rueckgabeWert == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			choosedFile = file.getAbsoluteFile();
			System.out.println("  choose FCW file: "+file.getName());
		}
		else {
			System.out.println("  Exit: no FCW file choosed");
			System.exit(0);
		}
			  
		File sourceFCW=choosedFile;
		File sourceZIP=new File(choosedFile.getPath().replaceAll(".fcw", ".zip"));
		File sourceSVG=new File(unzipPath+"\\page0.svg");
		File sourceXML=new File(unzipPath+"\\page0.xml");
			  
		// ------ change extension from fcw to zip ------
		Utile.changeExtention(sourceFCW, sourceZIP);
		// ------ unzip file ------
		Utile.unzip(sourceZIP, new File(unzipPath));
		
		// ------ change extension of page1 from svg to xml ------
		Utile.changeExtention(sourceSVG, sourceXML);
		
		// ------ change extension from zip to fcw ------
		Utile.changeExtention(sourceZIP, sourceFCW);
			  
		/**
		 * ===========================================================================
		 *  ------------------------- Parse SVG/XML  -------------------------------- 
		 * ===========================================================================
		 **/
		GUI_ProgressBar gui_ProgressBar = new GUI_ProgressBar();
		gui_ProgressBar.setProgressCount( " read data records from fcw file ");
			  
		Parser parse=new Parser();
		
		for(int i=1;i<parse.getOperators().size()+1;i++){
			try{
				P4parse.parse(i);
			}
			catch(Exception e){
				System.out.println("Layout "+(i+1)+" not fond");
			}
			System.out.println(i);
		}
		
		// System.exit(0);
		/**
		 * ===========================================================================
		 *  ----------------------- Upload Excel file  ------------------------------ 
		 * ===========================================================================
		 **/
				
			  
		new Excel(gui_ProgressBar,parse.getSequence(),parse.getLoads(),parse.getMachine(),parse.getOperators(),parse.getWalkingList(),parse.getTaktTime(),parse.getWorkstationTime());
			  
		// ------------- END -------------
		System.exit(0);
	}
}