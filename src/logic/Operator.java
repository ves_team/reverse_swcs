package logic;

import java.util.ArrayList;

public class Operator {
	private String color;
	private Integer cycle;
	private Double cycleMax;
	private ArrayList<Load> loads;
	
	public Operator(){
		cycle = 0;
		cycleMax=0.0;
		color=null;
		loads = new ArrayList<Load>();
	}
	public Operator(String pColor){
		color=pColor;
		cycle = 0;
		cycleMax=0.0;
		loads = new ArrayList<Load>();
	}
	
	public void print(int j){
		System.out.println("color :"+color);
		for (int i = 0; i < loads.size(); i++) {
			 System.out.println("in operator "+j+" load " + (i+1) + " :");
		     loads.get(i).print();
		}
	}
	
	//getter
	public Integer getCycle(){
		return cycle;
	}
	public Double getCycleMax(){
		return cycleMax;
	}
	public String getColor() {
		return color;
	}
	public ArrayList<Load> getLoads() {
		return loads;
	}
	//setter
	public void setCycleMax( Double cycleMax){
		this.cycleMax=cycleMax;
	}
	public void setCycle( Integer cycle){
		this.cycle=cycle;
	}
	public void setColor(String color) {
		this.color=color;
	}
	public void setLoad(Load load){
		this.loads.add(load);
	}
}
