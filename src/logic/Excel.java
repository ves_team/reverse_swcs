package logic;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.ShapeTypes;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSimpleShape;

public class Excel {
	GUI_ProgressBar gui_ProgressBar;
	ArrayList<Task> sequence;
	ArrayList<Load> loads;
	ArrayList<Load> machine;
	ArrayList<Operator> operators;
	ArrayList<Walking> WalkingList;
	ArrayList<Double> workstationTime;
	Double taktTime;
	
	
	// function to convert HEX to rgb color
	public static int[] getRGB(final String rgb){
		final int[] ret = new int[3];
		for (int i = 0; i < 3; i++){
			ret[i] = Integer.parseInt(rgb.substring(i * 2, i * 2 + 2), 16);
		}
		return ret;
	}
	
	public static void CloneCell(Cell cell, Cell cells){
		switch(cells.getCellTypeEnum()){
		case STRING:
			try{
				cell.setCellValue(cells.getStringCellValue());
			} catch(Exception e){}
			break;
			
		case NUMERIC:
			try{
			cell.setCellValue(cells.getNumericCellValue());
			} catch(Exception e){}
			break;
			
		case BLANK:
			try{
			cell.setCellType(CellType.BLANK);
			} catch(Exception e){}
			break;
			
		case BOOLEAN:
			try{
			cell.setCellValue(cells.getBooleanCellValue());
			} catch(Exception e){}
			break;
			
		case ERROR:
			try{
			cell.setCellErrorValue(cells.getErrorCellValue());
			} catch(Exception e){}
			break;
			
		case FORMULA:
			try{
			cell.setCellFormula(cells.getCellFormula());
			} catch(Exception e){}
			break;
			
		default:
			break;
			
		}
	}
	
	public static void LoadHeader(Workbook wb,Workbook wbs,int size){
		for (int j = 0; j <=size; j++) {
			Sheet sheet=wb.getSheetAt(j);
			Sheet sheetS=wbs.getSheetAt(0);
			
			// clone cell E4->E10
			for (int i = 3; i < 10; i++) {
				Cell cell=sheet.getRow(i).getCell(4);
				Cell cells=sheetS.getRow(i).getCell(4);
				 CloneCell(cell, cells);
				
			}
			// clone cell I7->I10
			for (int i = 6; i < 10; i++) {
				Cell cell=sheet.getRow(i).getCell(8);
				Cell cells=sheetS.getRow(i).getCell(8);
				CloneCell(cell, cells);
						
			}
			// clone cell N4->N13
			for (int i = 3; i < 13; i++) {
				Cell cell=sheet.getRow(i).getCell(13);
				Cell cells=sheetS.getRow(i).getCell(13);
				CloneCell(cell, cells);
						
			}
			
			//clone cell CY4->Cy8
			for (int i = 3; i < 8; i++) {
				Cell cell=sheet.getRow(i).getCell(102);
				Cell cells=sheetS.getRow(i).getCell(102);
				CloneCell(cell, cells);
						
			}
			
			//clone cell QQ4->QQ13
			for (int i = 3; i < 13; i++) {
				Cell cell=sheet.getRow(i).getCell(458);
				Cell cells=sheetS.getRow(i).getCell(458);
				CloneCell(cell, cells);
						
			}
			
		}
	}
	
	public Excel(GUI_ProgressBar gui_ProgressBar,ArrayList<Task> sequence,ArrayList<Load> loads,ArrayList<Load> machine,ArrayList<Operator> operators,ArrayList<Walking> WalkingList,Double taktTime,ArrayList<Double> workstationTime){
		
		this.gui_ProgressBar = gui_ProgressBar;
		this.sequence = sequence;
		this.loads = loads;
		this.machine = machine;
		this.operators = operators;
		this.WalkingList = WalkingList;
		this.taktTime = taktTime;
		this.workstationTime = workstationTime;
		
		
		ConfigFile conf = new ConfigFile();
		Integer firstRow = conf.getFirstRow();
		Integer firstCell = conf.getFirstCell();
		
		// choose folder
		File choosedFile = null;
		// JFileChooser dialog box
		JFileChooser fc = new JFileChooser(conf.getChoosePath());
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel files", "xlsm"); // show only xlsm files to choose
		fc.setFileFilter(filter);
		int rueckgabeWert = fc.showOpenDialog(null); // open dialog box and look for return statement
		  
			// if user choosed a valid file? 
		if(rueckgabeWert == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			choosedFile = file.getAbsoluteFile();
			System.out.println("  choose excel file: "+file.getName());
		}
		else {
			System.out.println("  Exit: no Excel file choosed");
			System.exit(0);
		}
		try {
			System.out.println("Openning Excel file...");
			gui_ProgressBar.setProgressCount("Openning Excel file...");
			//Workbook wb = new XSSFWorkbook(new FileInputStream(choosedFile));
			Workbook wbs = WorkbookFactory.create(new FileInputStream(choosedFile));
			Workbook wb = WorkbookFactory.create(new FileInputStream(conf.getTemplatePath()));
			System.out.println("opened");
			gui_ProgressBar.setProgressCount("excel file opened");
			
			LoadHeader(wb, wbs,0);
			wbs.close();
			
			String Outpath=choosedFile.getPath();
			
			System.out.println("Header loaded");
			gui_ProgressBar.setProgressCount("Header loaded");
			
			ArrayList<Sheet> sheetOpList = new ArrayList<Sheet>();
			for(int i=0;i<operators.size();i++){
				if(i!=0) sheetOpList.add(wb.cloneSheet(wb.getSheetIndex("P4-op1")));
				else sheetOpList.add(wb.getSheetAt(wb.getSheetIndex("P4-op1")));
			}
			
			System.out.println("sequence");
			gui_ProgressBar.setProgressCount("organise sequence on excel");
			//Sheet sheet=wb.cloneSheet(wb.getSheetIndex("template"));
			Sheet sheet=wb.getSheet("MMD");
			//String nameSheet=operators.size()+" op"+" tt "+taktTime;
			//wb.setSheetName(wb.getSheetIndex(sheet),nameSheet);
			/*Row title = sheet.getRow(conf.getTitleRow());
			Cell cell = title.getCell(conf.getTitleCell());
			cell.setCellValue("Organisation");*/
			for(int i=0;i<sequence.size();i++){
				Row row = sheet.getRow(i+firstRow);
				//Cell cellseq = row.getCell(conf.getSequenceCell());
				//cellseq.setCellValue((1+i)*10);
				Cell celldesc = row.getCell(conf.getDescripitionCell());
				celldesc.setCellValue(sequence.get(i).getDescription());
				Cell cellman = row.getCell(conf.getManualCell());
				cellman.setCellValue(sequence.get(i).getManualTime());
				Cell cellmac = row.getCell(conf.getMachineCell());
				cellmac.setCellValue(sequence.get(i).getMachineTime());
				Cell cellsec = row.getCell(3);
				cellsec.setCellValue(sequence.get(i).getMachineTime()+sequence.get(i).getManualTime());
				Cell cellRef = row.getCell(0);
				cellRef.setCellValue(i+1);
				Cell cellwait = row.getCell(13);
				cellwait.setCellValue(sequence.get(i).getWaitingTime());
				Cell cellLink = row.getCell(4);
				for (int j = 0; j < operators.size(); j++) {
					Boolean bool = false;
					for (int k = 0; k < operators.get(j).getLoads().size(); k++) {
						if(i == operators.get(j).getLoads().get(k).getIndex()){
							cellLink.setCellValue(operators.get(j).getLoads().get(k).getLink());
							bool=true;
							break;							
						}
					}
					if(bool) break;
				}
				Cell cellws = row.getCell(2);
				if(sequence.get(i).getWorkStation()==0) cellws.setCellValue("");
				else cellws.setCellValue(sequence.get(i).getWorkStation());
				Cell cellmov=row.getCell(conf.getMoveCell());
				if(sequence.get(i).getWalikingTime()==0) cellmov.setCellValue("");
				else cellmov.setCellValue(sequence.get(i).getWalikingTime());
			}
			System.out.println("op workimg time");
			for (int i = 0; i < operators.size(); i++) {
				Cell cellWT = sheet.getRow(i+3).getCell(141);
				cellWT.setCellValue(operators.get(i).getCycleMax());
			}
			System.out.println("machine workimg time");
			for (int i = 0; i < workstationTime.size(); i++) {
				Cell cellWT = sheet.getRow(i+3).getCell(158);
				cellWT.setCellValue(workstationTime.get(i));
			}
			System.out.println("machine");
			for (int i = 0; i < machine.size(); i++) {
				int[] rgb= new int[3];
				rgb=getRGB(machine.get(0).getColor().replaceAll("#", ""));
				XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();
				style.setAlignment(HorizontalAlignment.CENTER);
				style.setVerticalAlignment(VerticalAlignment.CENTER);
				style.setFillForegroundColor(new XSSFColor(new java.awt.Color(rgb[0],rgb[1],rgb[2])));   
				style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
				Font font = wb.createFont();
				font.setFontName("Calibri");
				font.setFontHeightInPoints((short) 8);
				style.setFont(font);
				Row row = sheet.getRow(machine.get(i).getIndex()+firstRow);
				int startI = (int) Math.round(machine.get(i).getStart()*10);
				for (int k = 0; k < machine.get(i).getTime()*10; k++) {
					Cell celln=row.getCell(firstCell+startI+k);
					celln.setCellValue("");
					celln.setCellStyle(style);
				}
			}
			System.out.println("walking");
			gui_ProgressBar.setProgressCount("add walking time on excel");
			for (int i = 0; i < WalkingList.size(); i++) {
				XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();
				style.setAlignment(HorizontalAlignment.CENTER);
				style.setVerticalAlignment(VerticalAlignment.CENTER);
				Font font = wb.createFont();
				font.setFontName("Calibri");
				font.setFontHeightInPoints((short) 8);
				style.setFont(font);
				style.setFillForegroundColor(new XSSFColor(new java.awt.Color(150,150,150)));   
				style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
				Row row = sheet.createRow(WalkingList.get(i).getLoadStart().getIndex()+firstRow);
				int startI = (int) Math.round(WalkingList.get(i).getStart()*10);
				for (int k = 0; k < WalkingList.get(i).getTime()*10; k++) {
					Cell celln=row.getCell(firstCell+startI+k);
					if(k==1) celln.setCellValue("");
					else celln.setCellValue("");
					celln.setCellStyle(style);
				}
			}
			System.out.println("organisation");
			gui_ProgressBar.setProgressCount("compute organisation on excel");
			for (int i = 0; i < operators.size(); i++) {
				int[] rgb= new int[3];
				rgb=getRGB(operators.get(i).getColor().replaceAll("#", ""));
				XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();
				style.setAlignment(HorizontalAlignment.CENTER);
				style.setVerticalAlignment(VerticalAlignment.CENTER);
				XSSFFont font = (XSSFFont) wb.createFont();
				font.setFontName("Calibri");
				font.setFontHeightInPoints((short) 8);
				font.setColor((short) 0);
				style.setFont(font);
				style.setFillForegroundColor(new XSSFColor(new java.awt.Color(rgb[0],rgb[1],rgb[2])));   
				style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
				for (int j = 0; j < operators.get(i).getLoads().size(); j++) {
					Row row = sheet.getRow(operators.get(i).getLoads().get(j).getIndex()+firstRow);
					int startI = (int) Math.round(operators.get(i).getLoads().get(j).getStart()*10);
					for (int k = 0; k < operators.get(i).getLoads().get(j).getTime()*10; k++) {
						Cell celln=row.getCell(firstCell+startI+k);
						celln.setCellValue("");
						celln.setCellStyle(style);
					}
					Cell cellOp = row.getCell(1);
					cellOp.setCellStyle(style);
					cellOp.setCellValue(i+1);
				}
			}
			// create takt time
			System.out.println("takt");
			gui_ProgressBar.setProgressCount("add takt on excel");
			if(taktTime!=0.0){
				Cell cellTT=sheet.getRow(12).getCell(13);
				cellTT.setCellValue(taktTime);
				Double max=0.0;
				for (int i = 0; i < sequence.size(); i++) {
					for (int j = 0; j < sequence.get(i).getLoads().size(); j++) {
						if(max<sequence.get(i).getLoads().get(j).getStart()+sequence.get(i).getLoads().get(j).getTime()){
							max=sequence.get(i).getLoads().get(j).getStart()+sequence.get(i).getLoads().get(j).getTime();
						}
					}
				}
				Double tt=0.0;
				if(max<taktTime) tt=1.0;
				else tt=(max/taktTime)+1;
				for (int i = 1; i < tt; i++) {
					CreationHelper helper = wb.getCreationHelper();
					Drawing<?> drawing = sheet.createDrawingPatriarch();
					
					ClientAnchor anchor = helper.createClientAnchor();
					anchor.setCol1((int) ((int) firstCell+taktTime*10*i));
					anchor.setRow1(firstRow); 
					anchor.setCol2((int) ((int) firstCell+taktTime*10*i));
					anchor.setRow2(firstRow+sequence.size()); 
					
					XSSFSimpleShape shape = ((XSSFDrawing)drawing).createSimpleShape((XSSFClientAnchor)anchor);
					shape.setShapeType(ShapeTypes.LINE);
					shape.setLineWidth(1.5);
					shape.setLineStyleColor(200,0,0);
				}
			}
			System.out.println("op-P4 sheet");
			gui_ProgressBar.setProgressCount("fulfile - P4 - operators\' sheets");
			// Fulfill op sheet
			for(int i=0;i<operators.size();i++){
				
				String nameSheet="P4-op"+(i+1);
				wb.setSheetName(wb.getSheetIndex(sheetOpList.get(i)),nameSheet);
				gui_ProgressBar.setProgressCount("creating sheet :"+(i+1)+"/"+(operators.size()));
				
				sheetOpList.get(i).getPrintSetup().setLandscape(true);
				sheetOpList.get(i).getPrintSetup().setPaperSize(HSSFPrintSetup.A4_PAPERSIZE);
				sheetOpList.get(i).setFitToPage(true);
				sheetOpList.get(i).getPrintSetup().setFitWidth( (short) 1);
				sheetOpList.get(i).getPrintSetup().setFitHeight( (short) 0);
				sheetOpList.get(i).setRepeatingRows(CellRangeAddress.valueOf("1:8"));
				

				// Fullfile P4 Header
				Cell cellopTT = sheetOpList.get(i).getRow(2).getCell(7);
				Cell cellopOp = sheetOpList.get(i).getRow(3).getCell(7);
				Cell cellopCy = sheetOpList.get(i).getRow(4).getCell(9);
				cellopTT.setCellValue(taktTime);
				cellopOp.setCellValue(i+1);
				cellopCy.setCellValue(operators.get(i).getCycleMax());
							
				ArrayList<CellStyle> newCellStyleList = new ArrayList<CellStyle>();
				for(int j=0;j<20;j++){
					CellStyle newCellStyle = wb.createCellStyle();
					newCellStyle.cloneStyleFrom(sheetOpList.get(i).getRow(10).getCell(j).getCellStyle());
					newCellStyleList.add(newCellStyle);
				}
				int cpt=0;
				for (int j = 0; j < operators.get(i).getCycle(); j++) {
					Row rowop=sheetOpList.get(i).getRow(0);
					if(j>9){
						sheetOpList.get(i).shiftRows(j+8, sheet.getLastRowNum()+1, 1, true,true);
						cpt++;
						rowop=sheetOpList.get(i).createRow(j+8);
						Row row0 = sheetOpList.get(i).getRow(8);
						rowop.setHeightInPoints(row0.getHeightInPoints());
						for(int k=0;k<20;k++){
							//System.out.println(">> boucle "+k);
							Cell newCell = rowop.createCell(k);
							newCell.setCellStyle(newCellStyleList.get(k));
							//System.out.println(newCellStyleList.get(k));
						}
					}
					if(j%10==0 && j!=0) sheetOpList.get(i).setRowBreak(j+7);
					
					try{
						rowop=sheetOpList.get(i).getRow(j+8);
						Cell cellopN = rowop.getCell(0);
						Cell cellopDesc = rowop.getCell(2);
						Cell cellopMan = rowop.getCell(7);
						//Cell cellopMac = rowop.getCell(8+offset);
						Cell cellopWS = rowop.getCell(1);
						//System.out.println(">>>>>>>>>>>"+j);
						cellopN.setCellValue(j+1);
						cellopDesc.setCellValue(sequence.get(operators.get(i).getLoads().get(j).getIndex()).getDescription());
						cellopMan.setCellValue(sequence.get(operators.get(i).getLoads().get(j).getIndex()).getManualTime());
						//cellopMac.setCellValue(sequence.get(operators.get(i).getLoads().get(j).getIndex()).getMachineTime());
						if(sequence.get(operators.get(i).getLoads().get(j).getIndex()).getWorkStation()!=0)
							cellopWS.setCellValue(sequence.get(operators.get(i).getLoads().get(j).getIndex()).getWorkStation());					
						
						for (int k = 0; k < WalkingList.size(); k++) {
							if(operators.get(i).getLoads().get(j).equals(WalkingList.get(k).getLoadStart())){
								Cell cellopMov=rowop.getCell(9);
								cellopMov.setCellValue(WalkingList.get(k).getTime());
							}
						}
					}catch(Exception e){}
				}
				if(operators.get(i).getCycle()>9 && operators.get(i).getCycle()%10!=0){
					for(int j=0;j<(10-operators.get(i).getCycle()%10);j++){
						//System.out.println(">>>>>>end>>>>>"+j);
						cpt++;
						sheetOpList.get(i).shiftRows(j+operators.get(i).getCycle()+8, sheet.getLastRowNum()+1, 1, true,true);
						Row rowBlank = sheetOpList.get(i).createRow(j+operators.get(i).getCycle()+8);
						Row row0 = sheetOpList.get(i).getRow(8);
						rowBlank.setHeightInPoints(row0.getHeightInPoints());
						for(int k=0;k<20;k++){
							//System.out.println(">> boucle end "+k);
							Cell newCellBlank = rowBlank.createCell(k);
							newCellBlank.setCellStyle(newCellStyleList.get(k));
							//System.out.println(newCellStyleList.get(k));
						}
					}
				}
				int nbrImg=operators.get(i).getCycle()/10+1;
				try{
					for(int j=0;j<nbrImg;j++){
						InputStream inputStream;
						//if(i==0) inputStream = new FileInputStream("C:\\SWCS\\Unzip\\Layout_0.png");
						/*else*/ inputStream = new FileInputStream("C:\\SWCS\\Unzip\\Layout_"+(i+1)+".png");
						//Get the contents of an InputStream as a byte[].
						byte[] bytes = IOUtils.toByteArray(inputStream);
						//Adds a picture to the workbook
						int pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
						//close the input stream
						inputStream.close();
						//Returns an object that handles instantiating concrete classes
					
						CreationHelper helper = wb.getCreationHelper();
						//Creates the top-level drawing patriarch.
						Drawing<?> drawing = sheetOpList.get(i).createDrawingPatriarch();
		
						//Create an anchor that is attached to the worksheet
						ClientAnchor anchor = helper.createClientAnchor();
		
						//create an anchor with upper left cell _and_ bottom right cell
						anchor.setCol1(12);
						anchor.setRow1(8+j*10);
						anchor.setCol2(19);
						anchor.setRow2(18+j*10);
		
						//Creates a picture
						Picture pict = drawing.createPicture(anchor, pictureIdx);
						
						//System.out.println("op : "+i+" img"+j);
						
					}
				}
				catch(Exception e){
					e.printStackTrace();
					JFrame PopUpWarningTemplate= new JFrame("JOptionPane showMessageDialog");
					JOptionPane.showMessageDialog(PopUpWarningTemplate,"Layout "+(i+1)+" not found","execel error", JOptionPane.ERROR_MESSAGE);
					
				}
				try{
					// footer logo
					InputStream is = new FileInputStream("C:\\SWCS\\logo.png");
					//Get the contents of an InputStream as a byte[].
					byte[] bs = IOUtils.toByteArray(is);
					//Adds a picture to the workbook
					int pictureIdx = wb.addPicture(bs, Workbook.PICTURE_TYPE_PNG);
					//close the input stream
					is.close();
					//Returns an object that handles instantiating concrete classes
				
					CreationHelper helper = wb.getCreationHelper();
					//Creates the top-level drawing patriarch.
					Drawing<?> drawing = sheetOpList.get(i).createDrawingPatriarch();
	
					//Create an anchor that is attached to the worksheet
					ClientAnchor anchor = helper.createClientAnchor();
	
					//create an anchor with upper left cell _and_ bottom right cell
					anchor.setCol1(0);
					anchor.setRow1(18+cpt);
					anchor.setCol2(2);
					anchor.setRow2(23+cpt);
					
					Picture logo = drawing.createPicture(anchor, pictureIdx);
					
					// footer
					is = new FileInputStream("C:\\SWCS\\footer.png");
					//Get the contents of an InputStream as a byte[].
					bs = IOUtils.toByteArray(is);
					//Adds a picture to the workbook
					pictureIdx = wb.addPicture(bs, Workbook.PICTURE_TYPE_PNG);
					//close the input stream
					is.close();
					//Returns an object that handles instantiating concrete classes
				
					helper = wb.getCreationHelper();
					//Creates the top-level drawing patriarch.
					drawing = sheetOpList.get(i).createDrawingPatriarch();
	
					//Create an anchor that is attached to the worksheet
					anchor = helper.createClientAnchor();
	
					//create an anchor with upper left cell _and_ bottom right cell
					anchor.setCol1(3);
					anchor.setRow1(20+cpt);
					anchor.setCol2(14);
					anchor.setRow2(23+cpt);
					
					Picture footer = drawing.createPicture(anchor, pictureIdx);
				}
				catch(Exception e){
					
				}
				
			}
			
			System.out.println("closing");
			gui_ProgressBar.setProgressCount("closing excel file");
			Outpath=Outpath.replaceAll(".xlsm", "-"+(operators.size())+"op tt "+taktTime)+".xlsm";
			FileOutputStream fos = new FileOutputStream(Outpath);
			wb.write(fos);
			fos.close();
					
			System.out.println("file \'output.xlsx\' generated without problems.");
			gui_ProgressBar.setFrameVisibility(false);
			JFrame PopUpCreated = new JFrame("JOptionPane showMessageDialog");
			JOptionPane.showMessageDialog(PopUpCreated, "Excel file createded ! \n"+Outpath);
			Desktop.getDesktop().open(new File(Outpath));
		}
		catch (IOException e) {
			e.printStackTrace();
			JFrame PopUpWarningTemplate= new JFrame("JOptionPane showMessageDialog");
			JOptionPane.showMessageDialog(PopUpWarningTemplate,e.getMessage(),"execel error", JOptionPane.ERROR_MESSAGE);
		}
		catch (Exception e) { 
			e.printStackTrace();
			JFrame PopUpWarningTemplate= new JFrame("JOptionPane showMessageDialog");
			JOptionPane.showMessageDialog(PopUpWarningTemplate,e.getMessage(),"execel error", JOptionPane.ERROR_MESSAGE);
		}
	}

}
