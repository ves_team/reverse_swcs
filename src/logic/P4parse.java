package logic;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Paths;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class P4parse {
public static void parse(Integer index) throws Exception {
    	
    	File source;
    	File destination;
    	source= new File("C:\\SWCS\\Unzip\\page"+index+".svg");
    	destination= new File("C:\\SWCS\\Unzip\\page"+index+".xml");
    	
    	try {
		  	source.renameTo(destination);
		}
		catch (Exception e) {
			e.printStackTrace();
			JFrame PopUpWarningTemplate= new JFrame("JOptionPane showMessageDialog");
			JOptionPane.showMessageDialog(PopUpWarningTemplate,e.getMessage(),"file error", JOptionPane.ERROR_MESSAGE);
		}
    	
    	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		org.w3c.dom.Document doc = dBuilder.parse(destination);
		doc.getDocumentElement().normalize();
		NodeList list = doc.getElementsByTagName("svg");
		Node nNode = list.item(0);
		Element eElement = (Element) nNode;
		eElement.setAttribute("xmlns", "http://www.w3.org/2000/svg");
		eElement.setAttribute("xmlns:xlink", "http://www.w3.org/2000/svg");
		eElement.setAttribute("doc_origin_x","72.07");
		eElement.setAttribute("doc_origin_y", "70.07");
		eElement.setAttribute("height", "998.35");
		eElement.setAttribute("width", "1260.08");

		
		Element defs = doc.createElement("defs");
		defs.appendChild(doc.createTextNode(" "));
		eElement.appendChild(defs);
		
		Element marker = doc.createElement("marker");
		marker.setAttribute("id", "arrow");
		marker.setAttribute("markerWidth", "10");
		marker.setAttribute("markerHeight", "10");
		marker.setAttribute("refX", "0");
		marker.setAttribute("refY", "3");
		marker.setAttribute("orient", "auto");
		marker.setAttribute("markerUnits", "strokeWidth");
		marker.appendChild(doc.createTextNode(" "));
		defs.appendChild(marker);
		
		Element path = doc.createElement("path");
		path.setAttribute("d", "M0,0 L0,6 L9,3 z");
		path.setAttribute("fill", "#000000");
		marker.appendChild(path);
		
		NodeList lineList = doc.getElementsByTagName("line");
		for (int temp = 0; temp < lineList.getLength(); temp++) {
			Node nNode1 = lineList.item(temp);
			Element eElement1 = (Element) nNode1;
			//System.out.println(temp+"/"+lineList.getLength());
			try{
				if(eElement1.hasAttribute("marker-end"))
				eElement1.setAttribute("marker-end", "url(#arrow)" );
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
		int opNbr=0;
		NodeList textList = doc.getElementsByTagName("text");
		for (int temp = 0; temp < textList.getLength(); temp++) {
			Node nNode1 = textList.item(temp);
			if (nNode1.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement1 = (Element) nNode1;
				Double x,y = 0.0;
				x=Double.parseDouble(eElement1.getAttribute("transform").split(" ")[0].split(",")[0].replaceAll("translate", "0").replace('(', '0'));
				y=Double.parseDouble(eElement1.getAttribute("transform").split(" ")[0].split(",")[1].replace(')','0'))/10;
				
				if(1000.0 < x && x < 1100.0 && 0.0 < y && y <100.0){
					try{
						opNbr=Integer.parseInt(eElement1.getElementsByTagName("tspan").item(0).getTextContent());
					}catch (NumberFormatException e) {
						System.out.println("not a number ! " + e.getMessage());
						JFrame PopUpWarningTemplate= new JFrame("JOptionPane showMessageDialog");
						JOptionPane.showMessageDialog(PopUpWarningTemplate,e.getMessage(),"Parse error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		}
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source1 = new DOMSource(doc);
		StreamResult result = new StreamResult(new File("C:\\SWCS\\Unzip\\page"+index+".svg"));
		transformer.transform(source1, result);
		
        try {
        	String svg_URI_input = Paths.get("C:\\SWCS\\Unzip\\page"+index+".svg").toUri().toURL().toString();
            TranscoderInput input_svg_image = new TranscoderInput(svg_URI_input);        
            
            OutputStream png_ostream = new FileOutputStream("C:\\SWCS\\Unzip\\Layout_"+opNbr+".png");
            TranscoderOutput output_png_image = new TranscoderOutput(png_ostream);              
           
            PNGTranscoder my_converter = new PNGTranscoder();        
            
            my_converter.transcode(input_svg_image, output_png_image);
            
            png_ostream.flush();
            png_ostream.close(); 
            System.out.println("img end");
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        }
    }
}
