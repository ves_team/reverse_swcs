package logic;

public class Walking {
	Double time; // walking time
	Double startAt; // time start
	Double finishAt; //time end
	Double Xi; // x initial position
	Double Yi; // y initial position
	Double Xf; // x final position
	Double Yf; // y final position
	Load[] loads; // tab of Loads linked with this walking time
	
	public Walking(){
		time=0.0;
		startAt=0.0;
		finishAt=0.0;
		Xi=0.0;
		Yi=0.0;
		Xf=0.0;
		Yf=0.0;
		loads = new Load[2];
		loads[0]=null;
		loads[1]=null;
	}
	
	public Walking(Double Xi,Double Yi,Double Xf,Double Yf){
		this.time=0.0;
		startAt=0.0;
		finishAt=0.0;
		this.Xi=Xi;
		this.Yi=Yi;
		this.Xf=Xf;
		this.Yf=Yf;
		this.loads = new Load[2];
	}
	
	//getter
		public Double getTime(){
			return time;
		}
		public Double getStart(){
			return startAt;
		}
		public Double getFinish(){
			return finishAt;
		}
		public Double getStartX() {
			return Xi;
		}
		public Double getStartY() {
			return Yi;
		}
		public Double getEndX() {
			return Xf;
		}
		public Double getEndY() {
			return Yf;
		}
		public Load getLoadStart(){
			return loads[0];
		}
		public Load getLoadEnd(){
			return loads[1];
		}
		//setter
		public void setTime(Double time){
			this.time=time;
		}
		public void setStart(Double start){
			this.startAt=start;
		}
		public void setStartX(Double start){
			this.Xi=start;
		}
		public void setFinish(Double finish){
			this.finishAt=finish;
		}
		public void setLoadStart(Load load) {
			loads[0]=load;
		}
		public void setLoadEnd(Load load) {
			loads[1]=load;
		}

}
