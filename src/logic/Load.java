package logic;

import java.util.Comparator;

public class Load {
	//String description; // name of the task.
	private String type;  // machine walk manual 
	private Double time;
	private Double wait;
	private Double x;  // x position
	private Double y;  // y position
	private String d;  // description of the path
	private Double startAt; // start time
	private Integer ref; // ref
	private Integer link; // link - load before
	private String color;
	private Boolean active; // if it's before of after the task description
	private int index; // index of the sequence
	
	public Load(){
		index=0;
		type=null;
		time=0.0;
		wait=0.0;
		x=0.0;
		y=0.0;
		d=null;
		startAt=0.0;
		ref =0;
		link=0;
		color=null;
		active = false;
	}
	public Load(String d,String type,String color){
		index=0;
		this.type=type;
		time=0.0;
		wait=0.0;
		x=0.0;
		y=0.0;
		this.d=d;
		startAt=0.0;
		ref=0;
		link=0;
		this.color=color;
		active = false;
	}
	
	public void print(){
		//System.out.println("desciption :"+description);
		System.out.println("index : "+index);
		System.out.println("type :"+type);
		System.out.println("time :"+time);
		System.out.println("x :"+x);
		System.out.println("y :"+y);
		System.out.println("d :"+d);
		System.out.println("start : "+startAt);
		System.out.println("color :"+color);
		System.out.println("active : "+active+"\n");
	}
	
	//getter
	/*public String getDescription() {
		return description;
	}*/
	public String getType() {
		return type;
	}
	public Double getTime() {
		return time;
	}
	public Double getX() {
		return x;
	}
	public Double getY() {
		return y;
	}
	public String getD() {
		return d;
	}
	public Double getStart(){
		return startAt;
	}
	public Integer getRef(){
		return ref;
	}
	public Integer getLink(){
		return link;
	}
	public String getColor() {
		return color;
	}
	public Boolean getActive() {
		return active;
	}
	public int getIndex() {
		return index;
	}
	public Double getWait() {
		return wait;
	}
	//setter
	/*public void setDescription(String pDescr) {
		description=pDescr;
	}*/
	public void setType(String pType) {
		type=pType;
	}
	public void setTime(Double pTime) {
		time=pTime;
	}
	public void setWait(Double pTime){
		wait=pTime;
	}
	public void setX(Double pX) {
		x=pX;
	}
	public void setY(Double pY) {
		y=pY;
	}
	public void setD(String pD) {
		d=pD;
	}
	public void setStart(Double start){
		startAt=start;
	}
	public void setRef( int ref){
		this.ref=ref;
	}
	public void setLink( int link){
		this.link=link;
	}
	public void setColor(String pColor) {
		color=pColor;
	}
	public void setActive(Boolean pActive) {
		active=pActive;
	}
	public void setIndex(int i){
		this.index=i;
	}
	
	public static Comparator<Load> loadX = new Comparator<Load>() {
		@Override
		public int compare(Load s1, Load s2) {

		   int x1 = (int) Math.round(s1.getX());
		   int x2 = (int) Math.round(s2.getX());

		   return x1-x2;

	   }};
	

}
