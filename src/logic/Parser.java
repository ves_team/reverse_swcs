package logic;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import java.io.File;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import logic.ConfigFile;
import logic.Load;
import logic.Task;
import logic.Walking;
import logic.Operator;

public class Parser {
	Double timeUnity=1.0;
	String taktD="";
	Double taktTime=0.0;
	
	ArrayList<Task> sequence = new ArrayList<Task>();
	ArrayList<Load> loads = new ArrayList<Load>();
	ArrayList<Load> machine = new ArrayList<Load>();
	ArrayList<Operator> operators = new ArrayList<Operator>();
	ArrayList<Walking> WalkingList = new ArrayList<Walking>();
	ArrayList<Double> workstationTime = new ArrayList<Double>();
	
	public Parser() throws ParserConfigurationException, SAXException{
		
		ConfigFile config=new ConfigFile();
		try {
			File fXmlFile = new File(config.getUnzipPath()+"\\page0.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			
			// #### find all image => sequence definition
			
			NodeList imageList = doc.getElementsByTagName("image");
			for (int temp = 0; temp < imageList.getLength(); temp++) {
				Node nNode = imageList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					String str = ""+eElement.getAttribute("xml:id");
					String[] tab = new String[5];
					tab = str.split("\\.");
					if(tab[0].equals("sequence")&&tab[4].equals("image1")){
						try{
							Task task=new Task(tab[2],Integer.parseInt(tab[3]),Double.parseDouble(eElement.getAttribute("x")),Double.parseDouble(eElement.getAttribute("y"))
									,Double.parseDouble(eElement.getAttribute("height")),Double.parseDouble(eElement.getAttribute("width")));
							sequence.add(task);
						} catch(Exception e){
							Task task=new Task(tab[2],0,Double.parseDouble(eElement.getAttribute("x")),Double.parseDouble(eElement.getAttribute("y"))
									,Double.parseDouble(eElement.getAttribute("height")),Double.parseDouble(eElement.getAttribute("width")));
							sequence.add(task);
						}
						}
						if(!tab[0].equals("annotation")&&tab[2].equals("time_unity")&&Double.parseDouble(tab[0])!=0.0){
							timeUnity=Double.parseDouble(tab[0]);
						}
					}
				}
			
			// ### find all path => loads
			NodeList pathList = doc.getElementsByTagName("path");
			for (int temp = 0; temp < pathList.getLength(); temp++) {
				Node nNode = pathList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					String[] tmp = new String[8];
					tmp=eElement.getAttribute("d").split(" ");
					try{
						if(tmp.length == 0) break;
						if( Double.parseDouble(tmp[0].replace('M', '0'))>600 && Double.parseDouble(tmp[1])>230){
							if(!eElement.getAttribute("fill").equals("#f0f0f0")&&!eElement.getAttribute("fill").equals("#1a1aff")&&!eElement.getAttribute("fill").equals("#ffffe2")&&!eElement.getAttribute("fill").equals("none")
									&&!eElement.getAttribute("fill").equals("#c0c0c0")&&!eElement.getAttribute("fill").equals("#ff0000")&&sequence.get(0).getY()-50<Double.parseDouble(tmp[1])){
								if(eElement.getAttribute("xml:id").split("\\.")[0].equals("workstation")){
									for (int i = 0; i < sequence.size(); i++) {
										if((sequence.get(i).getX()-10<Double.parseDouble(tmp[0].replace('M', '0'))) && (Double.parseDouble(tmp[0].replace('M', '0'))<sequence.get(i).getX()+10)){
											String fill = eElement.getAttribute("fill");
											if (fill.equals(config.getWorkstationColor(1))) {
												sequence.get(i).setWorkStation(1);
											} else if (fill.equals(config.getWorkstationColor(2))) {
												sequence.get(i).setWorkStation(2);
											} else if (fill.equals(config.getWorkstationColor(3))) {
												sequence.get(i).setWorkStation(3);
											} else if (fill.equals(config.getWorkstationColor(4))) {
												sequence.get(i).setWorkStation(4);
											} else if (fill.equals(config.getWorkstationColor(5))) {
												sequence.get(i).setWorkStation(5);
											} else if (fill.equals(config.getWorkstationColor(6))) {
												sequence.get(i).setWorkStation(6);
											} else if (fill.equals(config.getWorkstationColor(7))) {
												sequence.get(i).setWorkStation(7);
											} else if (fill.equals(config.getWorkstationColor(8))) {
												sequence.get(i).setWorkStation(8);
											} else if (fill.equals(config.getWorkstationColor(9))) {
												sequence.get(i).setWorkStation(9);
											} else if (fill.equals(config.getWorkstationColor(10))) {
												sequence.get(i).setWorkStation(10);
											} else {
												sequence.get(i).setWorkStation(0);
											}
										}
									}
								}
								else if(eElement.getAttribute("fill").equals("#0000ff")){
									Load load=new Load(eElement.getAttribute("d"),"machine",eElement.getAttribute("fill"));
									Double time = ((Double.parseDouble(tmp[2].replace('L', '0'))-Double.parseDouble(tmp[0].replace('M', '0')))/(24/timeUnity));
									time = Math.round(time*10)/10.0;
									load.setTime(time);
									load.setX(Double.parseDouble(tmp[0].replace('M', '0')));
									load.setY(Double.parseDouble(tmp[1]));
									load.setStart(Math.round((load.getX()/(24/timeUnity))*10)/10.0);
									load.setActive(true);
									loads.add(load);
									}
								else {
									Load load=new Load(eElement.getAttribute("d"),"manual",eElement.getAttribute("fill"));
									Double time = ((Double.parseDouble(tmp[2].replace('L', '0'))-Double.parseDouble(tmp[0].replace('M', '0')))/(24/timeUnity));
									time = Math.round(time*10)/10.0;
									load.setTime(time);
									load.setX(Double.parseDouble(tmp[0].replace('M', '0')));
									load.setY(Double.parseDouble(tmp[1]));
									load.setActive(true);
									loads.add(load);
									}
								}
						}
						
						if(eElement.getAttribute("fill").equals("#ff0000")){
							Double time = ((Double.parseDouble(tmp[2].replace('L', '0'))-Double.parseDouble(tmp[0].replace('M', '0')))/(24/timeUnity));
							time = Math.round(time*10)/10.0;
							taktTime=time;
							taktD=eElement.getAttribute("d");
							
							}
						}catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			
			// #### find all text => modify loads' time 
			NodeList textList = doc.getElementsByTagName("text");
			for (int temp = 0; temp < textList.getLength(); temp++) {
				Node nNode = textList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					Double x,y = 0.0;
					x=Double.parseDouble(eElement.getAttribute("transform").split(" ")[0].split(",")[0].replaceAll("translate", "0").replace('(', '0'));
					y=Double.parseDouble(eElement.getAttribute("transform").split(" ")[0].split(",")[1].replace(')','0'))/10;
					for (int j = 0; j < loads.size(); j++) {
						String[] d = new String [8];
						d=loads.get(j).getD().split(" ");
						if(Double.parseDouble(d[0].replace('M', '0')) < x && x < Double.parseDouble(d[2].replace('L', '0')) &&
							Double.parseDouble(d[3].replace('L', '0')) < y && y < Double.parseDouble(d[5].replace('L', '0'))){
							try{
								loads.get(j).setTime(Double.parseDouble(eElement.getElementsByTagName("tspan").item(0).getTextContent()));
							}catch (NumberFormatException e) {
								JFrame PopUpWarningTemplate= new JFrame("JOptionPane showMessageDialog");
								JOptionPane.showMessageDialog(PopUpWarningTemplate,e.getMessage(),"Parse error", JOptionPane.ERROR_MESSAGE);
							}								
						}
					}
					/*
					for (int j = 0; j < sequence.size(); j++) {
						if(sequence.get(j).getX() < x && x < sequence.get(j).getX()+sequence.get(j).getWidth() &&
								sequence.get(j).getY() < y && y < sequence.get(j).getY()+sequence.get(j).getHeight()){
							try{
								sequence.get(j).setWorkStation(Integer.parseInt(eElement.getElementsByTagName("tspan").item(0).getTextContent()));
							}catch (NumberFormatException e) {
								System.out.println("not a number ! " + e.getMessage());
								JFrame PopUpWarningTemplate= new JFrame("JOptionPane showMessageDialog");
								JOptionPane.showMessageDialog(PopUpWarningTemplate,e.getMessage(),"Parse error", JOptionPane.ERROR_MESSAGE);
							}								
						}
					}*/
					String[] d = new String [8];
					d=taktD.split(" ");
					try{
						if(d.length!=0 && taktD.length()!=0){
							if(Double.parseDouble(d[0].replace('M', '0')) < x && x < Double.parseDouble(d[2].replace('L', '0')) &&
									Double.parseDouble(d[3].replace('L', '0')) < y && y < Double.parseDouble(d[5].replace('L', '0'))){
								try{
									taktTime=Double.parseDouble(eElement.getElementsByTagName("tspan").item(0).getTextContent());
								}catch (NumberFormatException e) {
									System.out.println("not a number ! " + e.getMessage());
									JFrame PopUpWarningTemplate= new JFrame("JOptionPane showMessageDialog");
									JOptionPane.showMessageDialog(PopUpWarningTemplate,e.getMessage(),"Parse error", JOptionPane.ERROR_MESSAGE);
								}
							}
						}
					} catch (Exception e){
						e.printStackTrace();
					}
				}
			}
			//System.out.println(" taktTime : "+taktTime);
			
			Collections.sort(sequence,Task.taskY);
			Collections.sort(loads,Load.loadX);
			
			if(loads.size()!=0){
				Double refX0=loads.get(0).getX();
				for (int i = 0; i <loads.size(); i++) {
					loads.get(i).setX(loads.get(i).getX()-refX0);
					loads.get(i).setStart(Math.round((loads.get(i).getX()/(24/timeUnity))*10)/10.0);
				}
			}
			
			// ## add loads to sequence.
			for (int i = 0; i < sequence.size(); i++) {
				Double y=sequence.get(i).getY();
				for (int j = 0; j < loads.size(); j++) {
					if(y-24<loads.get(j).getY() && loads.get(j).getY()<y+24){
						loads.get(j).setIndex(i);
						loads.get(j).setLink(i);
						loads.get(j).setRef(i+1);
						sequence.get(i).setLoad(loads.get(j));
					}
				}
			}
			
			for (int i = 0; i < sequence.size(); i++) {
				int cpt1=0,cpt2=0;
				Double machine = 0.0,manual=0.0;
				for (int j = 0; j < sequence.get(i).getLoads().size(); j++) {
					if(sequence.get(i).getLoads().get(j).getType().equals("machine")){
						machine+=sequence.get(i).getLoads().get(j).getTime();
						cpt1++;
					}
					if(sequence.get(i).getLoads().get(j).getType().equals("manual")){
						manual+=sequence.get(i).getLoads().get(j).getTime();
						cpt2++;
					}
				}
				if(cpt1!=0) sequence.get(i).setMachineTime(Math.round((machine/cpt1)*10)/10.0);
				else sequence.get(i).setMachineTime(0.0);
				if(cpt2!=0) sequence.get(i).setManualTime(Math.round((manual/cpt2)*10)/10.0);
				else sequence.get(i).setManualTime(0.0);
			}
			
			//## create machine loads list
			for (int i = 0; i <loads.size(); i++) {
				if(loads.get(i).getType().equals("machine")){
					machine.add(loads.get(i));
				}
			}
			// ## create 1 operator for each colors
			
			for(int i=0; i<7;i++){
				Operator operator = new Operator(config.getOperatorColor(i));
				operators.add(operator);
				//System.out.println("op : "+config.getOperatorColor(i));
			}
			
			for (int i = 0; i < loads.size(); i++) {
				Boolean bool = true;
				for (int j = 0; j < operators.size(); j++) {
					if(operators.get(j).getColor().equals(loads.get(i).getColor())){
						bool=false;
					}
				}
				if(bool&&loads.get(i).getType().equals("manual")&&loads.get(i).getActive()){
					Operator operator = new Operator(loads.get(i).getColor());
					operators.add(operator);
				}
			}
				
			// add loads to each operator
			for (int j = 0; j < operators.size(); j++) {
				for (int i = 0; i < loads.size(); i++) {
					if(operators.get(j).getColor().equals(loads.get(i).getColor())){
						operators.get(j).setLoad(loads.get(i));
					}
				}
				Collections.sort(operators.get(j).getLoads(),Load.loadX);
			}
			
			for (int j = 0; j < operators.size(); j++) {
				//System.out.println("op "+operators.get(j).getColor()+" size :"+operators.get(j).getLoads().size());
				
				if(operators.get(j).getLoads().size()==0){
					operators.remove(j);
					j--;
				}
			}
			
			//arrange x position
			for (int j = 0; j < operators.size(); j++) {
				Double X0=operators.get(j).getLoads().get(0).getX();
				if (X0<(24*config.getPrecision())) {
					for (int i = 0; i < operators.get(j).getLoads().size(); i++) {
						operators.get(j).getLoads().get(i).setX(operators.get(j).getLoads().get(i).getX()-X0);
						operators.get(j).getLoads().get(i).setStart(Math.round((operators.get(j).getLoads().get(i).getX()/(24/timeUnity))*10)/10.0);
					}
				}				
			}
			
			// #### find all line => Walking List definition
			NodeList lineList = doc.getElementsByTagName("line");
			for (int temp = 0; temp < lineList.getLength(); temp++) {
				Node nNode = lineList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					if(!eElement.getAttribute("x1").equals(eElement.getAttribute("x2")) && !eElement.getAttribute("y1").equals(eElement.getAttribute("y2"))){
						Walking walk=new Walking(Double.parseDouble(eElement.getAttribute("x1")),Double.parseDouble(eElement.getAttribute("y1")),
								Double.parseDouble(eElement.getAttribute("x2")),Double.parseDouble(eElement.getAttribute("y2")));
						WalkingList.add(walk);
						}
					}
				}
			// ### compute walking list ( connect with load,  set time, start time and end time)
			for (int i = 0; i < WalkingList.size(); i++) {
				Double t1,t2;
				t1=0.0;
				t2=0.0;
				for (int j = 0; j < loads.size(); j++) {
					String[] d = new String [8];
					d=loads.get(j).getD().split(" ");
					if(Double.parseDouble(d[0].replace('M', '0')) < WalkingList.get(i).getStartX() && WalkingList.get(i).getStartX() < Double.parseDouble(d[2].replace('L', '0')) &&
							Double.parseDouble(d[3].replace('L', '0')) < WalkingList.get(i).getStartY() && WalkingList.get(i).getStartY() < Double.parseDouble(d[5].replace('L', '0'))){
						WalkingList.get(i).setLoadStart(loads.get(j));
						t1=(loads.get(j).getX()/(24/timeUnity))+loads.get(j).getTime();
					}
					if(Double.parseDouble(d[0].replace('M', '0')) < WalkingList.get(i).getEndX() && WalkingList.get(i).getEndX() < Double.parseDouble(d[2].replace('L', '0')) &&
							Double.parseDouble(d[3].replace('L', '0')) < WalkingList.get(i).getEndY() && WalkingList.get(i).getEndY() < Double.parseDouble(d[5].replace('L', '0'))){
						WalkingList.get(i).setLoadEnd(loads.get(j));
						t2=loads.get(j).getX()/(24/timeUnity);
					}
				}
				WalkingList.get(i).setStart(t1);
				WalkingList.get(i).setFinish(t2);
				if (0>Math.round((t2-t1)*10)/10.0){
					Load loadTmp=WalkingList.get(i).getLoadStart();
					WalkingList.get(i).setLoadStart(WalkingList.get(i).getLoadEnd());
					WalkingList.get(i).setLoadEnd(loadTmp);
					WalkingList.get(i).setTime(-Math.round((t2-t1)*10)/10.0);
				}
				else WalkingList.get(i).setTime(Math.round((t2-t1)*10)/10.0);
			}
			
			// arrange sequence
			for (int j = 0; j < operators.size(); j++) {
				for (int i = 1; i < operators.get(j).getLoads().size(); i++) {
					if(operators.get(j).getLoads().get(i).getX()-(24*config.getPrecision())<operators.get(j).getLoads().get(i-1).getX()+operators.get(j).getLoads().get(i-1).getTime()*(24/timeUnity)&&
							operators.get(j).getLoads().get(i-1).getX()+operators.get(j).getLoads().get(i-1).getTime()*(24/timeUnity)<operators.get(j).getLoads().get(i).getX()+(24*config.getPrecision())) {
						int start=(int) Math.round((operators.get(j).getLoads().get(i-1).getStart()+operators.get(j).getLoads().get(i-1).getTime())*10);
						operators.get(j).getLoads().get(i).setStart(operators.get(j).getLoads().get(i-1).getStart()+operators.get(j).getLoads().get(i-1).getTime());
					}
					if(operators.get(j).getLoads().get(i).getX()-(24*config.getPrecision())<operators.get(j).getLoads().get(i-1).getX()&&
							operators.get(j).getLoads().get(i-1).getX()<operators.get(j).getLoads().get(i).getX()+(24*config.getPrecision())) {
						int start=(int) Math.round((operators.get(j).getLoads().get(i-1).getStart()+operators.get(j).getLoads().get(i-1).getTime())*10);
						operators.get(j).getLoads().get(i).setStart(operators.get(j).getLoads().get(i-1).getStart());
					}
					//adjust of walking time
					for (int k = 0; k < WalkingList.size(); k++) {
						if(WalkingList.get(k).getLoadStart().getColor().equals(operators.get(j).getLoads().get(i).getColor())
								&&WalkingList.get(k).getLoadStart().getX().equals(operators.get(j).getLoads().get(i).getX())){
							WalkingList.get(k).setStart(operators.get(j).getLoads().get(i).getStart()+operators.get(j).getLoads().get(i).getTime());
							WalkingList.get(k).setTime(Math.round((WalkingList.get(k).getFinish()-WalkingList.get(k).getStart())*10)/10.0);
						}
						if(WalkingList.get(k).getLoadEnd().getColor().equals(operators.get(j).getLoads().get(i).getColor())
								&&WalkingList.get(k).getLoadEnd().getX().equals(operators.get(j).getLoads().get(i).getX())){
							operators.get(j).getLoads().get(i).setStart(WalkingList.get(k).getLoadStart().getStart()+WalkingList.get(k).getLoadStart().getTime()+WalkingList.get(k).getTime());
						}
					}
	
				}
			}
			
			// add walking on seq
			for (int i = 0; i < WalkingList.size(); i++) {
				sequence.get(WalkingList.get(i).getLoadStart().getIndex()).setWalking(WalkingList.get(i).getTime());
			}
			
			//Find Cycle of operation for each operators
			for (int i = 0; i < operators.size(); i++) {
				Boolean ok=false;
				int cycle=1;
				while(!ok){
					for (int j = 0; j < operators.get(i).getLoads().size(); j++) {
						if(operators.get(i).getLoads().get(j).getIndex()!=operators.get(i).getLoads().get(j%cycle).getIndex()){
							ok=false;
							break;
						}
						else ok=true;
					}
					cycle++;
				}
				//System.out.println("op: "+i+" cycle="+cycle);
				operators.get(i).setCycle(cycle-1);
			}
			
			//fiind critical cycle
			for (int i = 0; i < operators.size(); i++) {
				Double max=0.0;
				for (int j = 0; j < operators.get(i).getCycle(); j++) {
					if(operators.get(i).getLoads().get(j).getStart()+operators.get(i).getLoads().get(j).getTime()>max){
						max=operators.get(i).getLoads().get(j).getStart()+operators.get(i).getLoads().get(j).getTime();
					}
				}
				//System.out.println("op: "+i+" max="+max);
				operators.get(i).setCycleMax(max);
			}
			// Workstation use time
			ArrayList<Integer> ws = new ArrayList<Integer>();
			//ArrayList<Double> workstationTime = new ArrayList<Double>();
			for (int i = 0; i < sequence.size(); i++) {
				if(sequence.get(i).getWorkStation()!= null && sequence.get(i).getWorkStation()!= 0 ){
					Boolean in= false;
					for (int j = 0; j < ws.size(); j++) {
						in = sequence.get(i).getWorkStation()==ws.get(j);
					}
					if(!in) ws.add(sequence.get(i).getWorkStation()); //System.out.println(sequence.get(i).getWorkStation());
				}
			}
			for (int i = 0; i < ws.size(); i++) {
				Double time =0.0;
				for (int j = 0; j < sequence.size(); j++) {
					time = (sequence.get(j).getWorkStation()==ws.get(i)) ? time+sequence.get(j).getMachineTime()+sequence.get(j).getManualTime() : time;
				}
				//System.out.println(">>>>>>>>>>>>>>>>>>>> time ws : "+time);
				workstationTime.add(time);
			}
			
			// add Link to operator load
			for (int i = 0; i < operators.size(); i++) {
				for (int j = 0; j < operators.get(i).getLoads().size(); j++) {
					operators.get(i).getLoads().get(j).setLink((j%operators.get(i).getLoads().size()==0) ? 0 : operators.get(i).getLoads().get(j-1).getRef());
				}
			}
			for (int i = 0; i < operators.size(); i++) {
				for (int j = 0; j < operators.get(i).getLoads().size(); j++) {
					ArrayList<Integer> masque = new ArrayList<Integer>();
					Load masqueBy=new Load();
					for (int k = 0; k < operators.get(i).getLoads().size(); k++) {
						if(j!=k){
							Double startJ = operators.get(i).getLoads().get(j).getStart();
							Double startK = operators.get(i).getLoads().get(k).getStart();
							Double finishJ = operators.get(i).getLoads().get(j).getStart()+operators.get(i).getLoads().get(j).getTime();
							Double finishK = operators.get(i).getLoads().get(k).getStart()+operators.get(i).getLoads().get(k).getTime();
							if(startJ<=startK && finishK<finishJ){
								//System.out.println("Masque !! :"+operators.get(i).getLoads().get(k).getRef()+"j :"+j+"k :"+k);
								masque.add(k);
								masqueBy=operators.get(i).getLoads().get(j);
							}
						}
					}
					Boolean parallel = (j+1 != operators.get(i).getLoads().size() && operators.get(i).getLoads().get(j+1).getStart()-operators.get(i).getLoads().get(j).getStart()<config.getPrecision() );
										
					if(masque.size()!=0){
						for (int l = 0; l < masque.size(); l++) {
							operators.get(i).getLoads().get(masque.get(l)).setLink((l==0) ? masqueBy.getLink() : operators.get(i).getLoads().get(masque.get(l-1)).getRef() );
							
							//System.out.println("seq : "+operators.get(i).getLoads().get(masque.get(l)).getIndex()+" waiting :" + sequence.get(operators.get(i).getLoads().get(masque.get(l)).getIndex()).getWaitingTime());
						}
						try{
							operators.get(i).getLoads().get(masque.get(masque.size()-1)+1).setLink(masqueBy.getRef());
						}catch(Exception e){}
					}
					
					if(parallel) {
						operators.get(i).getLoads().get(j+1).setLink(operators.get(i).getLoads().get(j).getLink());
					}
				}
			}
			for (int i = 0; i < operators.size(); i++) {
				ArrayList<Integer> mask = new ArrayList<Integer>();
				for (int j = 0; j < operators.get(i).getLoads().size(); j++) {
					Double wait=0.0;
					Double min=null;
					for (int k = j+1; k < operators.get(i).getLoads().size(); k++) {
						Double startk = operators.get(i).getLoads().get(k).getStart();
						Double endk = operators.get(i).getLoads().get(k).getStart()+operators.get(i).getLoads().get(k).getTime();
						Double endj = operators.get(i).getLoads().get(j).getStart()+operators.get(i).getLoads().get(j).getTime();
						 if(endk>endj){
							 wait=startk-endj;
							 //System.out.printf("waiting calculating (%d,%d) : %f \n",i,k,wait);
							 wait = (wait< config.getPrecision()) ? 0.0 : wait;
							 
							 if (min==null) min = wait;
							 min = Math.min(min, wait);
							 //System.out.printf("min calculating (%d,%d) : %f \n",i,k,min);
						 }
						 else {
							 mask.add(k);
						 }
						 
					}
					 if (min==null) min=0.0;
					//System.out.printf("waiting time recalculate (%d,%d) : %f \n",i,j,min);
					 sequence.get(operators.get(i).getLoads().get(j).getIndex()).setWaitingTime(min);
					 
				}
				for (int k = 0; k < mask.size(); k++) {
					//System.out.println("masque "+k+" "+mask.get(k));
					sequence.get(operators.get(i).getLoads().get(mask.get(k)).getIndex()).setWaitingTime(0.0);
				}
				
			}
			
			for (int k = 0; k < WalkingList.size(); k++) {
				sequence.get(WalkingList.get(k).getLoadStart().getIndex()).setWaitingTime(0.0);
			}
			
		}
		catch (IOException e) { 
			e.printStackTrace();
			JFrame PopUpWarningTemplate= new JFrame("JOptionPane showMessageDialog");
			JOptionPane.showMessageDialog(PopUpWarningTemplate,e.getMessage(),"parsing error", JOptionPane.ERROR_MESSAGE);
			}
		catch(Exception e){
			JFrame PopUpWarningTemplate= new JFrame("JOptionPane showMessageDialog");
			JOptionPane.showMessageDialog(PopUpWarningTemplate,"error during parsing some data may be missing","parsing error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	public Double getTaktTime(){
		return taktTime;
	}
	public ArrayList<Task> getSequence(){
		return sequence;
	}
	public ArrayList<Load> getLoads(){
		return loads;
	}
	public ArrayList<Load> getMachine(){
		return machine;
	}
	public ArrayList<Operator> getOperators(){
		return operators;
	}
	public ArrayList<Walking> getWalkingList(){
		return WalkingList;
	}
	public ArrayList<Double> getWorkstationTime(){
		return workstationTime;
	}
	

}
