package logic;

import java.util.ArrayList;
import java.util.Comparator;

import org.apache.poi.ss.usermodel.Workbook;

public class Task {
	private String description; // descrpition of the task
	private Integer workStation;
	private Double x; // x position
	private Double y; // y position
	private Double height; 
	private Double width;
	private Double manualTime;
	private Double machineTime;
	private Double walkingTime;
	private Double waitingTime;
	private Double cpt;
	private Double cpt2;
	private ArrayList<Load> loads; //list of Loads associate to this task
	
	public Task() {
		description=null;
		workStation=null;
		y=0.0;
		x=0.0;
		height=0.0;
		width=0.0;
		manualTime=0.0;
		machineTime=0.0;
		walkingTime=0.0;
		waitingTime=0.0;
		cpt=cpt2=0.0;
		loads = new ArrayList<Load>();
	}
	
	public Task(String Descr,Integer workStation,Double X,Double Y, Double H,Double W) {
		this.description=Descr;
		this.workStation=workStation;
		this.y=Y;
		this.x=X;
		this.height=H;
		this.width=W;
		this.manualTime=0.0;
		this.machineTime=0.0;
		this.walkingTime=0.0;
		this.waitingTime=0.0;
		cpt=cpt2=0.0;
		this.loads = new ArrayList<Load>();
	}
	
	public void print(int j){
		System.out.println("description :"+description);
		System.out.println("x :"+x);
		System.out.println("y :"+y);
		System.out.println("height :"+height);
		System.out.println("width :"+width);
		System.out.println("manualTime :"+manualTime);
		System.out.println("machineTime :"+machineTime);
		System.out.println("walkingTime :"+walkingTime+"\n");
		for (int i = 0; i < loads.size(); i++) {
			 System.out.println("in task "+j+" load " + (i+1) + " :");
		     loads.get(i).print();
		}
	}

	
		//getter
		public String getDescription() {
			return description;
		}
		public Integer getWorkStation(){
			return workStation;
		}
		public Double getX() {
			return x;
		}
		public Double getY() {
			return y;
		}
		public Double getHeight() {
			return height;
		}
		public Double getWidth() {
			return width;
		}
		public Double getManualTime() {
			return manualTime;
		}
		public Double getMachineTime() {
			return machineTime;
		}
		public Double getWalikingTime() {
			return walkingTime;
		}
		public Double getWaitingTime() {
			return waitingTime;
		}
		public ArrayList<Load> getLoads() {
			return loads;
		}
		
		//setter
		public void setDescription(String pDescription) {
			description=pDescription;
		}
		public void setWorkStation(Integer workSation){
			this.workStation=workSation;
		}
		public void setX(Double pX){
			y=pX;
		}
		public void setY(Double pY){
			y=pY;
		}
		public void setwidth(Double pWidth){
			width=pWidth;
		}
		public void setHeight(Double pHeight){
			height=pHeight;
		}
		public void setMachineTime(Double pTime){
			machineTime=pTime;
		}
		public void setManualTime(Double pTime){
			manualTime=pTime;
		}
		public void setWalking(Double pTime){
			walkingTime=Math.round(((pTime+(walkingTime*cpt))/(cpt+1.0))*10)/10.0;
			cpt=cpt+1.0;
		}
		public void setWaitingTime(Double pTime){
			if(pTime!=0.0){	
				waitingTime=Math.round(((pTime+(waitingTime*cpt2))/(cpt2+1.0))*10)/10.0;
				cpt2=cpt2+1.0;
			} else waitingTime=0.0;
		}
		public void setLoad(Load pLoad){
			loads.add(pLoad);
		}
		
		public static Comparator<Task> taskY = new Comparator<Task>() {
			@Override
			public int compare(Task s1, Task s2) {

			   int y1 = (int) Math.round(s1.getY());
			   int y2 = (int) Math.round(s2.getY());

			   return y1-y2;

		   }};

}
