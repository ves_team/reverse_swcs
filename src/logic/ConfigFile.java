package logic;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigFile {
	
	private String unzipPath;
	
	private String templatePath;
	private String choosePath;
	
	private Integer firstRow;
	private Integer firstCell;
	
	private Double precision;
	
	private String[] operatorColor;
	
	private String[] workstationColor;
	
	private Integer titleRow,titleCell,moveCell,machineCell,manualCell,descrCell,seqCell,operatorCell;
    
    /**
     * Loads configuration parameters from a textfile and print them out.
     */
	public ConfigFile() {
 
        // load configuration file
    	Properties prop = new Properties();
    	InputStream input = null;
    	String filename = "config.properties";
    	
    	workstationColor=new String[11];
    	operatorColor=new String[7];
    	
    	try {
    		// assume properties file �config.properties� is in your project classpath root folder.
    		input = ConfigFile.class.getClassLoader().getResourceAsStream(filename);
    		if (input == null){

    			// config file not loaded
    			System.out.println("  load standard parameters - no config file found");
    		}
    		
    		// load a properties file from class path, inside static method
    		prop.load(input);
    		
        	// set parameters from config file to variables (found in config, default)
    		unzipPath=prop.getProperty("path_unzip", "C:\\swcs\\unzip");
        	templatePath = prop.getProperty("path_template", "C:\\swcs\\template.xlsx");
        	choosePath = prop.getProperty("path_chooseFolder","UNKNOW");
        	
        	firstRow = Integer.parseInt( prop.getProperty("first_row", "1"));
        	firstCell = Integer.parseInt(prop.getProperty("first_column", "1"));
        	titleRow = Integer.parseInt(prop.getProperty("title_row","null"));
        	titleCell = Integer.parseInt(prop.getProperty("title_cell","null"));
        	moveCell = Integer.parseInt(prop.getProperty("move_cell","null"));
        	seqCell = Integer.parseInt(prop.getProperty("sequence_cell","null"));
        	machineCell = Integer.parseInt(prop.getProperty("machine_cell","null"));
        	manualCell = Integer.parseInt(prop.getProperty("manual_cell","null"));
        	descrCell = Integer.parseInt(prop.getProperty("description_cell","null"));
        	operatorCell = Integer.parseInt(prop.getProperty("operator_cell","null"));
        	
        	precision=Double.parseDouble(prop.getProperty("precision","1"));
        	
        	for (int i = 1; i < operatorColor.length+1; i++) {
        		operatorColor[i-1]=prop.getProperty("op"+i);
			}
        	
        	for (int i = 0; i < workstationColor.length; i++) {
				workstationColor[i]= "#"+prop.getProperty("ws"+i);
			}
        	
        	
            	
        	// config file loaded
        	System.out.println("  load parameters from config file");
        	
    	} catch (IOException ex) {
    		ex.printStackTrace();
        } finally{
        	if(input!=null) {
        		try {
        			input.close();
        		} catch (IOException e) {
        			e.printStackTrace();
        		}
        	}
        }
    }
	
	
	 // Getters	

	public Integer getFirstRow() {
		return firstRow;
	}
	public Integer getFirstCell() {
		return firstCell;
	}
	public String getTemplatePath() {
		return templatePath;
	}
	public String getChoosePath(){
		return choosePath;
	}
	public String getUnzipPath(){
		return unzipPath;
	}
	public String getOperatorColor(int i){
		return operatorColor[i];
	}
	public String getWorkstationColor(int i){
		return workstationColor[i];
	}
	public Integer getTitleRow(){
		return titleRow;
	}
	public Integer getTitleCell(){
		return titleCell;
	}
	public Integer getMoveCell(){
		return moveCell;
	}
	public Integer getMachineCell(){
		return machineCell;
	}
	public Integer getManualCell(){
		return manualCell;
	}
	public Integer getDescripitionCell(){
		return descrCell;
	}
	public Integer getOperatorCell(){
		return operatorCell;
	}
	public Integer getSequenceCell(){
		return seqCell;
	}
	public Double getPrecision(){
		return precision;
	}
	
}
